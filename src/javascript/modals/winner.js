import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';
import { NAME } from '../constants/fighterPropertiesNames';


export function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(winner) {
  let newLine = '\n';
  const { name, source } = winner;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const winnerName = createElement({ tagName: 'span', className: 'winner-name' });
  const winnerImage = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });

  winnerName.innerText = NAME + name + newLine;

  winnerDetails.append(winnerName);
  winnerDetails.append(winnerImage);

  return winnerDetails;
}