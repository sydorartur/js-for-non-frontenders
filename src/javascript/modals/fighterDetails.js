import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { NAME, ATTACK, DEFENSE, HEALTH } from '../constants/fighterPropertiesNames';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  let newLine = '\n';
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });

  nameElement.innerText = NAME + name;
  attackElement.innerText = ATTACK + attack;
  defenseElement.innerText = DEFENSE + defense;
  healthElement.innerText = HEALTH + health + newLine;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
