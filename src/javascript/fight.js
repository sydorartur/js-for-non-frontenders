export function fight(firstFighter, secondFighter) {
  let counter = 0;
  let firstClone = createFiterClone(firstFighter);
  let secondClone = createFiterClone(secondFighter);
  while (isBothAlive(firstClone, secondClone)) {
    if (counter % 2 === 0) {
      playRound(firstClone, secondClone);
    } else {
      playRound(secondClone, firstClone);
    }
    counter++;
  }
  return determineWinner(firstClone, firstFighter, secondFighter);
}

function createFiterClone(fighter) {
  return {
    name: fighter.name,
    health: fighter.health,
    attack: fighter.attack,
    defense: fighter.defense
  };
}

function determineWinner(firstClone, firstFighter, secondFighter) {
  if (firstClone.health > 0) {
    return firstFighter;
  } else {
    return secondFighter;
  }
}

function playRound(attacker, enemy) {
  let damage = getDamage(attacker, enemy);
  enemy.health -= damage;
}

function isBothAlive(firstFighter, secondFighter) {
  return firstFighter.health > 0 && secondFighter.health > 0;
}

export function getDamage(attacker, enemy) {
  let hit = getHitPower(attacker);
  let block = getBlockPower(enemy);
  let damage = hit - block;
  if(damage < 0) {
    damage = 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  let criticalHitChance = getRandom();
  let hitPower = attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  let dodgeChance = getRandom();
  let blockPower = defense * dodgeChance;
  return blockPower;
}

function getRandom() {
  let min = 1;
  let max = 2;
  let random = Math.random() * (+max - +min) + +min;
  return random;
}
